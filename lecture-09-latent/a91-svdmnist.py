import glob, os, shutil
import numpy as np
import scipy.linalg as la
from bootstrap import *	
from metrics import *
from matplotlib.pyplot import *
from sklearn.naive_bayes import GaussianNB

pct_reconstruction = 0.60

data = np.loadtxt("mnist4k.csv", delimiter=",")
imgs = data[:,1:]
labels = data[:,0]    

print "loaded images ",imgs.shape
print "fitting SVD..."
(U,s,V)=la.svd(imgs)

# ----- YOUR CODE HERE -----
# copmute the index of s that acounts for 60% of variability
reduction_index = ???
# --------------------------

# reduce U,s,V according to reduction index
rU = U[:,0:reduction_index]
rs = np.diag(s[0:reduction_index])
rV = V[0:reduction_index,:]

print "rU ",rU.shape, "; rs ",rs.shape, "; rV ",rV.shape

classifier = GaussianNB()

# ---- YOUR CODE HERE ----
# use the gaussian classifier to classify first the original pixels and then
# the transformed data (in rU)
# use the bootstrap_score function that you completed in a previous exercise
# with 10 boostraps, a test_size of 0.5 and the accuracy metric (compute_ACC)
#
# report for each case (original pixels and transformed data) the mean and stdev
# of accuracy in train and in test
# -----------------------

# interpret rV as eigen imgs
print "saving eigen imgs ...."
dir = "/tmp/mnist-eigenimgs"
shutil.rmtree(dir, ignore_errors=True)
os.makedirs(dir)    
i=0
for base_img in rV:
    if i>20:
        break
    imshow(base_img.reshape(28,28), aspect="equal", interpolation="nearest", cmap = cm.Greys_r)
    savefig(dir+"/img"+str(i)+".jpg")    
    i += 1

# reconstruct imgs
dir="/tmp/mnist-reconstructed"
shutil.rmtree(dir, ignore_errors=True)
os.makedirs(dir)
print "reconstructing imgs ...."
reconstructed_imgs = [np.array([f]).dot(rs).dot(rV)[0] for f in rU]

print "saving reconstructed imgs ..."
i=0
for f in reconstructed_imgs:
    if i>10:
        break
    imshow(f.reshape(28,28), aspect="equal", interpolation="nearest", cmap = cm.Greys_r)
    savefig(dir+"/img"+str(i)+"r.jpg")
    imshow(imgs[i].reshape(28,28), aspect="equal", interpolation="nearest", cmap = cm.Greys_r)
    savefig(dir+"/img"+str(i)+"o.jpg")          
    i += 1
