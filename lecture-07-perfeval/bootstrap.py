import numpy as np
from sklearn import cross_validation
from sklearn.metrics import accuracy_score

def bootstrap_score(classifier, data, targets, score_function=accuracy_score, nb_bootstraps=5, test_size=0.2):
    
    scores_train = []
    scores_test  = []
    for i in np.arange(0, nb_bootstraps):

        data_train, data_test, targets_train, targets_test = cross_validation.train_test_split(data, targets, test_size=test_size, random_state=np.random.randint(1,100))
    
        classifier.fit (data_train, targets_train)
        score_train = score_function(targets_train, classifier.predict(data_train))
        score_test  = score_function(targets_test, classifier.predict(data_test))
        scores_train.append(score_train)
        scores_test.append(score_test)

    return np.vstack((scores_train, scores_test))

def bootstrap_score_string(bs_score):
    means, stds = np.mean(bs_score, axis=1), np.std(bs_score, axis=1)
    return "tr %0.2f(%0.3f), ts %0.2f(%0.3f)"%(means[0], stds[0], means[1], stds[1])
