import numpy as np
from lr import *
from sklearn.datasets import *
from plotdata import *

def quad_features(X):
    return np.hstack((X, X**2))

n=200
(d,c) = make_moons(n_samples=n, noise=0.2)
lr = LogisticRegression(0.005, map_features_function=quad_features)
lr.fit(d,c)
print "circles accuracy train ="+str(lr.score(d,c))
plot_boundary_with_data(lr,d,c)
show()
    
