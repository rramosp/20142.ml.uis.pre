import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("data-classification.csv", delimiter=",")
X = data[:,0:2]
y = data[:,2]
X0 = X[y==0]
X1 = X[y==1]
plt.scatter(X0[:,0], X0[:,1], color="r")
plt.scatter(X1[:,0], X1[:,1], color="b")

px = np.arange(-1.5,2.5,0.1)
py = .26+.1*px
plt.plot(px, py)

# it might help you to know the number of points in X
n = X.shape[0]

# -------- define t as the theta vector
# t = 

# -------- add a column of ones to X
# X =

# -------- compute and print the number of errors
# errors = 
# print "Number of errors", errors)
plt.show()
