import numpy as np
from lro import *
from sklearn.datasets import *
from plotdata import *
from matplotlib.pyplot import *

def quad_features(X):
    return np.hstack((X, X**2))

def polynomial_features(X):
    x = np.array([X[:,0]]).T
    y = np.array([X[:,1]]).T
    
    r = x
    for i in range(1,7):
        for j in range(0,i+1):
            r = np.hstack((r,x**(i-j)*y**j))
    return r   

n=120
train_data = np.loadtxt(open("xtrain-2.csv"), delimiter=",")
train_labels = np.loadtxt(open("ytrain-2.csv"), delimiter=",")
test_data = np.loadtxt(open("xtest-2.csv"), delimiter=",")
test_labels = np.loadtxt(open("ytest-2.csv"), delimiter=",")
"""
(train_data,train_labels) = make_moons(n_samples=n, noise=0.15)
(test_data, test_labels)  = make_moons(n_samples=n, noise=0.15)

np.savetxt(open("xtrain-2.csv","w"), train_data, delimiter=",")
np.savetxt(open("ytrain-2.csv","w"), train_labels)
np.savetxt(open("xtest-2.csv","w"), test_data, delimiter=",")
np.savetxt(open("ytest-2.csv","w"), test_labels)
"""

lambda_list = [0.0, 0.1, 5.0]
i=1
for l in lambda_list:
    lr = LogisticRegressionO(0.001, map_features_function=polynomial_features, lmbd=l)
    lr.fit(train_data, train_labels)
    
    w_norm =np.sqrt(np.sum(lr.theta**2))
    train_acc = lr.score(train_data, train_labels)
    test_acc = lr.score(test_data, test_labels)
    
    print "train acc = "+str(train_acc)
    print "test acc  = "+str(test_acc)
    
    subplot(2,3,i+3)
    title ("test l=%0.2f"%l+" acc=%0.3f"%test_acc)
    plot_boundary_with_data(lr,test_data, test_labels)
    subplot(2,3,i)    
    title ("train l=%0.2f"%l+" acc=%0.3f"%train_acc)
    plot_boundary_with_data(lr,train_data, train_labels)
    i=i+1
    
show()
    



