import numpy as np
from lr import *
from sklearn.datasets import *
from plotdata import *
from matplotlib.pyplot import *

def polynomial_features(X):
    x = np.array([X[:,0]]).T
    y = np.array([X[:,1]]).T
    
    r = x
    for i in range(1,7):
        for j in range(0,i+1):
            r = np.hstack((r,x**(i-j)*y**j))
    return r   


train_data = np.loadtxt(open("xtrain-1.csv"), delimiter=",")
train_labels = np.loadtxt(open("ytrain-1.csv"), delimiter=",")
test_data = np.loadtxt(open("xtest-1.csv"), delimiter=",")
test_labels = np.loadtxt(open("ytest-1.csv"), delimiter=",")

lambda_list = np.arange(0.0,0.1,0.001)
test_accuracies = []
train_accuracies = []
w_norms = []
for l in lambda_list:
    print "lambda regularization = "+str(l)

    # ---- INSERT YOUR CODE HERE
    # 
    # - fit logistic regression using train data with alpha=0.001 and polynomial features
    lr = ??????

    # - compute test and train accuracies to add them to the train_accuries and test_accuracies vectors
    test_acc = ????
    train_acc = ????
    test_accuracies = ????
    train_accuracies = ????

    # - compute the norm of theta and add it to the w_norms vector. computed theta is accessible at lr.theta
    w_norm = ?????
    w_norms = ?????
    # -------------------------

print train_accuracies
print test_accuracies
print w_norms

# ---- INSERT YOUR CODE HERE ---
#
# - plot train and test accuracies as a function of lambda
# - plot w_norm as a function of lambda
# ------------------------------

show()

