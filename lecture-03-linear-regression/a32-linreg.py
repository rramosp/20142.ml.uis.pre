import numpy as np
from numpy.linalg import pinv
from decimal import *

x = np.loadtxt(open("x.dat"))
y = np.loadtxt(open("y.dat"))

m = y.shape[0]
mu=x.mean(axis=0)
sigma=x.std(axis=0)

# --- MODIFY HERE scale each column by (x-mu)/sigma
x = (x-mu)/sigma

# --- MODIFY HERE add a column on ones to x 
x = np.hstack((np.ones((x.shape[0],1)),x))


alpha=1.0
theta = np.zeros(x.shape[1])
max_iter = 100

for i in range(0,max_iter):
    # --- MODIFY HERE --- write the formula for the gradient
    grad = ????
    delta = alpha * grad

    # --- MODIFY HERE --- break loop if the norm squared of delta is less than 0.01
    if ??????:
        break
    
    # -- this is the actual update
    theta = theta - delta

    # --- print progress
    print i, theta

# --- MODIFY HERE --- compute the mean squared error
mse =  ?????

print "mse                            ", mse
print "number of iterations           ", i
print "theta                          ", theta

# --- MODIFY HERE estimate the price of a 1640 sq feet, 3 beedrom house
new_house = np.array([1650, 3])
