# Machine Learning 2014.2 #
## Course materials

This is the course material for the Machine Learning course held on 2014 semester 2 at Undergraduate Computer Science, at Universidad Industrial de Santander, Bucaramanga Colombia.

Visit the course home page [https://sites.google.com/site/rulixrp/courses/2014-2-machine-learning-pregrado-ingenieria-sistemas-uis](https://sites.google.com/site/rulixrp/courses/2014-2-machine-learning-pregrado-ingenieria-sistemas-uis).
